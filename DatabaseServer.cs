﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DatabaseEngines;
using System.Xml.Serialization;


namespace DiagramEngine
{
    [XmlTypeAttribute(Namespace = "urn:xmlns:25hoursaday-com:my-DatabaseServer")]
    [XmlRootAttribute("DatabaseServer", Namespace = "urn:xmlns:25hoursaday-com:my-DatabaseServer", IsNullable = false)]
    public class DatabaseServer
    {
        [XmlIgnoreAttribute]
        public string IP{get; set;}
        [XmlIgnoreAttribute]
        public string Database { get; set; }
        [XmlIgnoreAttribute]
        public string Username { get; set; }
        [XmlIgnoreAttribute]
        public string Password { get; set; }
        [XmlIgnoreAttribute]
        public bool Connected { get; set; }

        public DatabaseEngineEnum Engine { get; set; }
        
        [XmlIgnoreAttribute]
        public SQLEngine tDatabaseServer;

        [XmlArray("Tables"), XmlArrayItem("Table", typeof(Table))] 		    
		public List<Table> Tables{ get; set; }

        public DatabaseServer()
        {
            Engine = DatabaseEngineEnum.MSSQL;
        }

        public DatabaseServer(DatabaseEngineEnum engine)
        {
            Engine = engine;
        }
        
        public bool Connect()
        {
            switch (Engine)
            {
                case DatabaseEngineEnum.MSSQL:
                    {
                        MSSQLEngine databaseServer = new MSSQLEngine();
                        databaseServer.IP = IP;
                        databaseServer.Username = Username;
                        databaseServer.Password = Password;
                        databaseServer.Database = Database;
                        tDatabaseServer = databaseServer;
                        break;
                    }
                case DatabaseEngineEnum.MySQL:
                    {
                        MySQLEngine databaseServer = new MySQLEngine();
                        databaseServer.IP = IP;
                        databaseServer.Username = Username;
                        databaseServer.Password = Password;
                        databaseServer.Database = Database;
                        tDatabaseServer = databaseServer;
                        break;
                    }
                case DatabaseEngineEnum.Oracle:
                    {
                        OracleEngine databaseServer = new OracleEngine();
                        databaseServer.IP = IP;
                        databaseServer.Username = Username;
                        databaseServer.Password = Password;
                        databaseServer.Database = Database;
                        tDatabaseServer = databaseServer;
                        break;
                    }
            }

            try
            {
                tDatabaseServer.Connect();
                this.Tables = tDatabaseServer.Tables;
                Connected = true;
                return Connected;
            }
            catch (Exception e)
            {
                string r = e.Message;
                Connected = false;
                return Connected;
            }
        }

    }
}
