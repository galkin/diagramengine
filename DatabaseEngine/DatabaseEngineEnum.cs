﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseEngines
{
    [Serializable]
    public enum DatabaseEngineEnum
    {
        MSSQL,
        MySQL,
        Oracle
    }
}
