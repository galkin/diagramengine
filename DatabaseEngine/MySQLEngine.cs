﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using System.Xml.Serialization;

namespace DatabaseEngines
{
    [Serializable]
    [XmlInclude(typeof(MySQLEngine))]
    class MySQLEngine : SQLEngine
    {
        public override bool Connect()
        {
            string ConnectString = String.Format(@"server={0}; user id={1}; password = {2}; database={3}; pooling=false;", IP, Username, Password, Database);

            MySqlConnection mySqlConnection = new MySqlConnection(ConnectString);
           
            try
            {
                mySqlConnection.Open();
                Connected = true;
                Tables = GetTables(mySqlConnection);
                return true;
            }
            catch(Exception e)
            {
                Connected = false;
                throw e;
            }
        }

        public override List<Field> GetFields(string Name, object connect)
        {
            DataSet ds;
            List<Field> arrFields = new List<Field>();
            string sqlText = @"DESCRIBE `@name`";

            try
            {
                ds = GetDataSet(sqlText, connect, Name);
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Field f = new Field();
                        f.Name = row[0].ToString();
                        f.Type = row[1].ToString();
                        arrFields.Add(f);
                    }
                }
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }

            return arrFields;
        }

        public override List<Table> GetTables(object connect)
        {
            DataSet ds;
            List<Table> arrTables = new List<Table>();
            const string sqlText = @"SHOW TABLES";

            try
            {
                ds = GetDataSet(sqlText, connect, null);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Table table = new Table();
                    //table.Name = row[0].ToString();
                    //table.Fields = GetFields(table.Name, connect);
                    arrTables.Add(table);
                }
            }
            catch (Exception e)
            {
                string g = e.Message;
                return null;
            }

            return arrTables;
        }

        public override List<Reference> GetReferences(string name, object connect)
        {            
            return null;
        }

        private DataSet GetDataSet(string sqlText, object connect, string param)
        {
            DataSet ds = new DataSet();

            using (MySqlCommand sqlCommand = new MySqlCommand(sqlText, (MySqlConnection)(connect)))
            {

                if (param != null)
                {
                    sqlCommand.Parameters.Add(new MySqlParameter("name", param));

                }
                sqlCommand.Prepare();
                MySqlDataAdapter da = new MySqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }

            return ds;
        }
    }
}
