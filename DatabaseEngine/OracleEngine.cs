﻿using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.SqlClient;

namespace DatabaseEngines
{
    public class OracleEngine : SQLEngine
    {
        private int VersionEngine;

        public override bool Connect()
        {
            string ConnectString = string.Format(@"User Id= {0};Password={1};DBA Privilege=SYSDBA", Username, Password);

            OracleConnection con = new OracleConnection(ConnectString);

            try
            {
                con.Open();
                Connected = true;

                VersionEngine = GetVersion(con);

                Tables = GetTables(con);
                return true;
            }
            catch
            {
                Connected = false;
                return false;
            }
        }

        public int GetVersion(object connect)
        {
            return 10;
        }

        public override List<Field> GetFields(string Name, object connect)
        {
            DataSet ds;
            List<Field> arrFields = new List<Field>();

            string sqlText = @"SELECT COLUMN_NAME, DATA_TYPE FROM user_tab_columns WHERE table_name = :name";

            try
            {
                ds = GetDataSet(sqlText, connect, Name);
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Field f = new Field();
                        f.Name = row["COLUMN_NAME"].ToString();
                        f.Type = row["DATA_TYPE"].ToString();
                        arrFields.Add(f);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return arrFields;
        }

        public override List<Table> GetTables(object connect)
        {
            DataSet ds;
            List<Table> arrTables = new List<Table>();
            string sqlText = @"SELECT object_name FROM user_objects WHERE object_type = 'TABLE' ORDER BY object_name";
            
            try
            {
                ds = GetDataSet(sqlText, connect, null);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Table table = new Table();
                    table.Name = row["object_name"].ToString();
                    table.Fields = GetFields(table.Name, connect);
                    table.References = GetReferences(table.Name, connect);
                    arrTables.Add(table);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return arrTables;
        }

        public override List<Reference> GetReferences(string name, object connect)
        {
            DataSet ds;
            List<Reference> arrReferences = new List<Reference>();
            string sqlText = @"SELECT d1.constraint_type, d2.table_name ForeignTable,
                                      d2.table_name parent_table, c1.column_name ForeignKeyField
                               FROM dba_constraints d1, dba_constraints d2, all_cons_columns c1
                               WHERE d1.table_name = :name
                                 AND d1.r_constraint_name = d2.constraint_name
                                 AND d1.constraint_name = c1.constraint_name
                                 AND d1.table_name = c1.table_name";
            
            try
            {
                ds = GetDataSet(sqlText, connect, name);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Reference reference = new Reference();

                    reference.ForeignTable = row["ForeignTable"].ToString();
                    reference.ForeignField = row["ForeignKeyField"].ToString();
                    reference.PrimaryTable = name;
                    reference.PrimaryField = "test";//row["PrimaryKeyField"].ToString();

                    arrReferences.Add(reference);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return arrReferences;
        }

        private DataSet GetDataSet(string sqlText, object connect, string param)
        {
            DataSet ds = new DataSet();

            using (OracleCommand sqlCommand = new OracleCommand(sqlText, (OracleConnection)(connect)))
            {

                if (param != null)
                {
                    sqlCommand.Parameters.Add(":name", OracleDbType.Varchar2);
                    sqlCommand.Parameters[":name"].Value = param;
                }

                OracleDataAdapter da = new OracleDataAdapter(sqlCommand);
                da.Fill(ds);
            }

            return ds;
        }
    }
}
