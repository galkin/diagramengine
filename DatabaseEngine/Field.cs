﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DatabaseEngines
{
    [Serializable]
    public class Field
    {
        [XmlAttributeAttribute("Name")]
        public string Name { get; set; }
        [XmlAttributeAttribute("Type")]
        public string Type { get; set; }
    }
}
