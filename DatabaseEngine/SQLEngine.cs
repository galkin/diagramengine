﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Serialization;

namespace DatabaseEngines
{
    [Serializable]
    [XmlInclude(typeof(MSSQLEngine))]
    [XmlInclude(typeof(MySQLEngine))]
    public abstract class SQLEngine
    {
        public string IP { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Connected { get; set; }

        public abstract bool Connect();

        public List<Table> Tables { get; set; }

        public abstract List<Field> GetFields(string Name, object connect);

        public abstract List<Table> GetTables(object connect);

        public abstract List<Reference> GetReferences(string name, object connect);
    }
}
