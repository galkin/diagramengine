﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DatabaseEngines
{
    [Serializable]
    public class Table
    {
        [XmlAttributeAttribute("Name")]		
        public string Name { get; set; }
        [XmlArray("Fields"), XmlArrayItem("Field", typeof(Field))]    
		public List<Field> Fields { get; set; }
        [XmlArray("References"), XmlArrayItem("Reference", typeof(Reference))] 		
        public List<Reference> References { get; set; }
    }
}
