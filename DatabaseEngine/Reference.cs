﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseEngines
{
    [Serializable]
    public class Reference
    {
        public string PrimaryField { get; set; }
        public string PrimaryTable { get; set; }
        public string ForeignField { get; set; }
        public string ForeignTable { get; set; }
    }
}
