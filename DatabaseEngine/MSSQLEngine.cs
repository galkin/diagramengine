﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace DatabaseEngines
{
    public class MSSQLEngine : SQLEngine
    {
        private int VersionEngine;

        public override bool Connect()
        {
            string ConnectString = string.Format("Data source={0};Initial catalog={1};User ID={2};password={3}", IP, Database, Username, Password);

            SqlConnection sqlConnection = new SqlConnection(ConnectString);
            try
            {
                sqlConnection.Open();
                Connected = true;

                VersionEngine = GetVersion(sqlConnection);

                Tables = GetTables(sqlConnection);
                return true;
            }
            catch
            {
                Connected = false;
                return false;
            }
        }

        public int GetVersion(object connect)
        {
            DataSet ds;
            string sqlText = @"SELECT CONVERT(INT, SUBSTRING(CONVERT(NVARCHAR(10), SERVERPROPERTY('productversion')), 1, 1))";
            int i = 0;

            try
            {
                ds = GetDataSet(sqlText, connect, null);
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        i = (int) row[0];
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return i;
        }

        public override List<Field> GetFields(string Name, object connect)
        {
            DataSet ds;
            List<Field> arrFields = new List<Field>();
            string sqlText = string.Empty;

            if (VersionEngine == 9)
            {
                sqlText =
                    @"SELECT c.name AS field_name, TYPE_NAME(system_type_id) AS field_type 
                      FROM sys.columns c 
                      INNER JOIN sys.objects o ON c.object_id = o.object_id 
                      WHERE o.name = @name";
            }
            else
            {
                sqlText = @"SELECT c.name AS field_name, t.name AS field_type 
                            FROM syscolumns c
                            INNER JOIN systypes t ON c.xtype = t.xtype
                            WHERE id = OBJECT_ID(@name)";
            }

            try
            {
                ds = GetDataSet(sqlText, connect, Name);
                if (ds.Tables[0].Rows.Count >= 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Field f = new Field();
                        f.Name = row["field_name"].ToString();
                        f.Type = row["field_type"].ToString();
                        arrFields.Add(f);
                    }
                }
            }
            catch (Exception e)
            {
                string m = e.Message;
                return null;
            }

            return arrFields;
        }

        public override List<Table> GetTables(object connect)
        {
            DataSet ds;
            List<Table> arrTables = new List<Table>();
            string sqlText = String.Empty;

            if (VersionEngine == 9)
            {

                sqlText = @"SELECT name FROM sys.objects WHERE type = 'U' ORDER BY name";
            }
            else
            {
                sqlText = @"SELECT name FROM sysobjects WHERE type = 'U' ORDER BY name";
            }

            try
            {
                ds = GetDataSet(sqlText, connect, null);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Table table = new Table();
                    table.Name = row["name"].ToString();
                    table.Fields = GetFields(table.Name, connect);
                    table.References = GetReferences(table.Name, connect);                    
                    arrTables.Add(table);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return arrTables;
        }

        public override List<Reference> GetReferences(string name, object connect)
        {
            DataSet ds;
            List<Reference> arrReferences = new List<Reference>();
            string sqlPrimaryText;
            string sqlForeignText = "";

            if (VersionEngine >= 9)
            {
                sqlPrimaryText = @"SELECT 
                              COL_NAME(parent_object_id, parent_column_id) AS PrimaryKeyField,
                              COL_NAME(referenced_object_id, referenced_column_id) AS ForeignKeyField,
                              OBJECT_NAME(referenced_object_id) AS ForeignTable
                            FROM    sys.foreign_key_columns
                            WHERE   parent_object_id = OBJECT_ID(@name)";

                sqlForeignText = @"SELECT 
                              COL_NAME(parent_object_id, parent_column_id) AS PrimaryKeyField,
                              COL_NAME(referenced_object_id, referenced_column_id) AS ForeignKeyField,
                              OBJECT_NAME(parent_object_id) AS PrimaryTable
                            FROM    sys.foreign_key_columns
                            WHERE   referenced_object_id = OBJECT_ID(@name)";
            }
            else
            {
                // for SQL 7, SQL 2000
                #region SQL code
                sqlPrimaryText = @"SELECT 
                              ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                              ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                              PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
                            FROM  sysreferences r
                            INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
                            INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
                            INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey1
                            INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey1
                            WHERE r.rkeyid = OBJECT_ID(@name)
                            UNION ALL
                          SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                            ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                            PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
                          FROM  sysreferences r
                          INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
                          INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
                          INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey2
                          INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey2
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey3
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey3
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey4
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey4
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
         SELECT ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey5
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey5
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
         SELECT ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey6
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey6
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
         SELECT ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey7
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey7
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey8
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey8
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey9
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey9
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey10
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey10
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey11
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey11
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey12
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey12
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey13
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey13
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey14
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey14
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey15
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey15
        WHERE r.rkeyid = OBJECT_ID(@name)
        UNION ALL
        SELECT  ForeignKeyField = CONVERT(NVARCHAR(150), c1.name),
                ForeignTable = CONVERT(NVARCHAR(150), o2.name),
                PrimaryKeyField = CONVERT(NVARCHAR(150), c2.name)
        FROM  sysreferences r
        INNER JOIN sysobjects o1 ON o1.id = r.rkeyid
        INNER JOIN sysobjects o2 ON o2.id = r.fkeyid
        INNER JOIN syscolumns c1 ON c1.id = r.rkeyid AND c1.colid = r.rkey16
        INNER JOIN syscolumns c2 ON c2.id = r.fkeyid AND c2.colid = r.fkey16
        WHERE r.rkeyid = OBJECT_ID(@name)";
                #endregion
            }

            try
            {
                //Primary
                ds = GetDataSet(sqlPrimaryText, connect, name);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Reference reference = new Reference();

                    reference.ForeignTable = row["ForeignTable"].ToString();
                    reference.ForeignField = row["ForeignKeyField"].ToString();
                    reference.PrimaryTable = name;
                    reference.PrimaryField = row["PrimaryKeyField"].ToString();
                    
                    arrReferences.Add(reference);
                }
                //Foreign
                ds = GetDataSet(sqlForeignText, connect, name);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Reference reference = new Reference();

                    reference.ForeignTable = name;
                    reference.ForeignField = row["ForeignKeyField"].ToString();
                    reference.PrimaryTable = row["PrimaryTable"].ToString();
                    reference.PrimaryField = row["PrimaryKeyField"].ToString();

                    arrReferences.Add(reference);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return arrReferences;
        }

        private DataSet GetDataSet(string sqlText, object connect, string param)
        {
            DataSet ds = new DataSet();

            using (SqlCommand sqlCommand = new SqlCommand(sqlText, (SqlConnection) (connect)))
            {

                if (param != null)
                {
                    sqlCommand.Parameters.Add("@name", SqlDbType.NVarChar);
                    sqlCommand.Parameters["@name"].Value = param;
                }

                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                da.Fill(ds);
            }

            return ds;
        }
    }
}
