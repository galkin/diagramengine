﻿using System;
using System.Windows.Forms;
using DatabaseEngines;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace DiagramEngine
{
    public partial class DiagramEngine : Form
    {
        DatabaseServer databaseServer;

        public DiagramEngine()
        {
            InitializeComponent();
        }

        public Object DeserializeObject(Object pObject)
        {
            try
            {
                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(pObject.GetType());

                // A FileStream is needed to read the XML document.
                FileStream fs =
                    new FileStream(String.Format(Application.StartupPath + @"\{0}.xml", pObject.GetType().Name),
                                   FileMode.Open);
                XmlReader reader = new XmlTextReader(fs);
                // Use the Deserialize method to restore the object's state.
                return serializer.Deserialize(reader);

                // Write out the properties of the object.
            }
            catch (InvalidOperationException ioe)
            {
                MessageBox.Show(ioe.InnerException.Message, "XML Serialization Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        } 

        public void SerializeObject(Object pObject)
        {
            try
            {
                StreamWriter w = new StreamWriter(String.Format(Application.StartupPath + @"\{0}.xml", pObject.GetType().Name));
                XmlSerializer s = new XmlSerializer(pObject.GetType());
                s.Serialize(w, pObject);
                w.Close();
            }
            catch (InvalidOperationException ioe)
            {
                MessageBox.Show(ioe.InnerException.Message, "XML Serialization Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            //http://andyorc:5500/em
            DatabaseEngineEnum type = DatabaseEngineEnum.MSSQL;
            //type = (DatabaseEngineEnum)(comboBox1.SelectedItem);
            switch(comboBox1.SelectedIndex)
            {
                case 0: {
                    type = DatabaseEngineEnum.MSSQL; break; }
                case 1: {
                    type = DatabaseEngineEnum.MySQL; break; }
                case 2: {
                    type = DatabaseEngineEnum.Oracle; break; }
            }

            databaseServer = new DatabaseServer(type);
            databaseServer.IP = textBoxServer.Text;
            databaseServer.Database = textBoxDatabase.Text;
            databaseServer.Username = textBoxUsername.Text;
            databaseServer.Password = textBoxPassword.Text;
            databaseServer.Connect();
            
            SerializeObject(databaseServer);

            DatabaseServer server1;

            server1 = (DatabaseServer) DeserializeObject(databaseServer);

            if (databaseServer.Connected)
            {
                //foreach (Table t in databaseServer.Tables)
                //{
                //    listView1.Items.Add(t.Name);
                //}
                
                MessageBox.Show("yes");
            }
            else
                MessageBox.Show("no");
            
        }
       
        private void listView2_Click(object sender, EventArgs e)
        {
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show(listView1.SelectedItems[0].ToString()); 
        }

        private void DiagramEngine_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
